## Post Resolution Summary

I started by understanding the scope of the exercise and then scanned the project structure
to have a clear vision of what was where. After that, I started writing unit tests for the
newly added actions to then write reducers to make those tests pass.
Finally, I focused on linking those new features to the UI through the itemsList component.

The last part is also the one I would have focused on if given more time. 
I would have separated single todo items form the itemsList component and refactored 
itemCreator into a form element to use the onSubmit event and therefore allow a better user experience.

## Tasks

We'd like you to make the following changes to the To Do app:

1. Add the ability to delete items.
2. Be able to mark items as complete. And then toggle them back to incomplete.
3. Add a filter that can be toggled to hide completed items.
