import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  deleteItem,
  toggleItem,
  toggleCompletedItems
} from '../../logic/actions';
import { filterVisibleItems } from '../../logic/selectors';
import './styles.css';

export const ItemsList = ({
  items,
  visibleItems,
  showCompletedItems,
  deleteItem,
  toggleItem,
  toggleCompletedItems
}) => {
  const itemsCount = items.length;
  const visibleItemsCount = visibleItems.length;
  const hiddenItemsCount = itemsCount - visibleItemsCount;
  const styleCompletedItem = {
    textDecoration: 'line-through',
    opacity: 0.6
  };
  const styleIncompleteItem = {
    textDecoration: 'none',
    opacity: 1
  };

  return (
    <div>
      <ul className={'itemsList-ul'}>
        {itemsCount < 1 && <p id={'items-missing'}>Add some tasks above.</p>}
        {visibleItems.map(item => (
          <li key={item.id}>
            <label>
              <input
                type="checkbox"
                checked={item.isComplete}
                onChange={() => toggleItem(item.id)}
              />
              <span
                style={
                  item.isComplete ? styleCompletedItem : styleIncompleteItem
                }
              >
                {item.content}
              </span>
            </label>
            <span>
              <button onClick={() => deleteItem(item.id)}>Delete</button>
            </span>
          </li>
        ))}
      </ul>

      {itemsCount > 0 && (
        <label>
          <input type="checkbox" onChange={toggleCompletedItems} />
          Hide completed items{' '}
          {showCompletedItems || hiddenItemsCount < 1
            ? ''
            : '(' + hiddenItemsCount + ' hidden)'}
        </label>
      )}
    </div>
  );
};

ItemsList.propTypes = {
  items: PropTypes.array.isRequired,
  visibleItems: PropTypes.array.isRequired
};

const mapStateToProps = state => {
  return {
    items: state.todos.items,
    visibleItems: filterVisibleItems(state.todos).items,
    showCompletedItems: state.todos.showCompletedItems
  };
};

const mapDispatchToProps = dispacth => {
  return {
    deleteItem: itemId => dispacth(deleteItem(itemId)),
    toggleItem: itemId => dispacth(toggleItem(itemId)),
    toggleCompletedItems: () => dispacth(toggleCompletedItems())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemsList);
