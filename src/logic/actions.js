import {
  ADD_ITEM,
  DELETE_ITEM,
  TOGGLE_ITEM,
  TOGGLE_COMPLETED_ITEMS
} from './constants';

export const addItem = content => {
  return { type: ADD_ITEM, content };
};

export const deleteItem = itemID => {
  return { type: DELETE_ITEM, itemID };
};

export const toggleItem = itemID => {
  return {
    type: TOGGLE_ITEM,
    itemID
  };
};

export const toggleCompletedItems = itemID => {
  return {
    type: TOGGLE_COMPLETED_ITEMS
  };
};
