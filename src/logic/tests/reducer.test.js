import reducer, { initialState } from '../reducer';
import { addItem, deleteItem, toggleItem, toggleCompletedItems } from '../actions';
import { filterVisibleItems } from '../selectors';

describe('reducer', () => {
  it('should return state for unknown action', () => {
    const mockState = { test: 'testItem' };
    const dispatchAction = { type: 'mystery-meat' };
    const result = reducer(mockState, dispatchAction);
    expect(result).toEqual(mockState);
  });

  it('should use initial state if state not provided', () => {
    const dispatchAction = { type: 'mystery-meat' };
    const result = reducer(undefined, dispatchAction);
    expect(result).toEqual(initialState);
  });

  it('should add the new item on ADD_ITEM', () => {
    const initialState = {
      items: [
        {
          id: 1,
          content: 'first',
          isComplete: false
        }
      ]
    };

    const expectedState = {
      items: [
        {
          id: 1,
          content: 'first',
          isComplete: false
        },
        {
          id: 2,
          content: 'second',
          isComplete: false
        }
      ]
    };

    const dispatchAction = addItem('second');
    const resultingState = reducer(initialState, dispatchAction);
    expect(resultingState).toEqual(expectedState);
  });

  it('should remove the item on DELETE_ITEM', () => {
    const initialState = {
      items: [
        {
          id: 1,
          content: 'first',
          isComplete: false
        },
        {
          id: 2,
          content: 'second',
          isComplete: false
        }
      ]
    };

    const expectedState = {
      items: [
        {
          id: 2,
          content: 'second',
          isComplete: false
        }
      ]
    };

    const dispatchAction = deleteItem(1);
    const resultingState = reducer(initialState, dispatchAction);
    expect(resultingState).toEqual(expectedState);
  });

  it("should toggle item's isComplete prop on TOGGLE_ITEM", () => {
    const initialState = {
      items: [
        {
          id: 1,
          content: 'first',
          isComplete: false
        },
        {
          id: 2,
          content: 'second',
          isComplete: false
        }
      ]
    };

    const expectedState = {
      items: [
        {
          id: 1,
          content: 'first',
          isComplete: true
        },
        {
          id: 2,
          content: 'second',
          isComplete: false
        }
      ]
    };

    const dispatchAction = toggleItem(1);
    const resultingState = reducer(initialState, dispatchAction);
    expect(resultingState).toEqual(expectedState);
  });

  it('should toggle showCompletedItems prop on TOGGLE_COMPLETED_ITEMS', () => {
    const initialState = {
      items: [],
      showCompletedItems: true
    };

    const expectedState = {
      items: [],
      showCompletedItems: false
    };

    const dispatchAction = toggleCompletedItems();
    const resultingState = reducer(initialState, dispatchAction);
    expect(resultingState).toEqual(expectedState);
  });
});

describe('selector', () => {
  it('should show all items if showCompletedItems is set to true', () => {
    const state = {
      items: [
        {
          id: 1,
          content: 'first',
          isComplete: true
        },
        {
          id: 2,
          content: 'second',
          isComplete: false
        }
      ],
      showCompletedItems: true
    };

    const expectedState = {
      ...state
    };

    const resultingState = filterVisibleItems(state);
    expect(resultingState).toEqual(expectedState);
  });

  it('should show only incomplete items if showCompletedItems is set to false', () => {
    const state = {
      items: [
        {
          id: 1,
          content: 'first',
          isComplete: true
        },
        {
          id: 2,
          content: 'second',
          isComplete: false
        }
      ],
      showCompletedItems: false
    };

    const expectedState = {
      items: [
        {
          id: 2,
          content: 'second',
          isComplete: false
        }
      ],
      showCompletedItems: false
    };

    const resultingState = filterVisibleItems(state);
    expect(resultingState).toEqual(expectedState);
  });
});
