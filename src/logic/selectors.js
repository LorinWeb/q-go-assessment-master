export const filterVisibleItems = state => {
  const items = state.showCompletedItems
    ? state.items
    : state.items.filter(item => !item.isComplete);

  return {
    ...state,
    items
  };
};
