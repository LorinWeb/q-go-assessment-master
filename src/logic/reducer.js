import {
  ADD_ITEM,
  DELETE_ITEM,
  TOGGLE_ITEM,
  TOGGLE_COMPLETED_ITEMS
} from './constants';

export const initialState = {
  items: [
    {
      id: 1,
      content: 'first',
      isComplete: true
    },
    {
      id: 2,
      content: 'second',
      isComplete: false
    }
  ],
  showCompletedItems: true
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_ITEM:
      const nextId =
        state.items.reduce((id, item) => Math.max(item.id, id), 0) + 1;

      return {
        ...state,
        items: [
          ...state.items,
          {
            id: nextId,
            content: action.content,
            isComplete: false
          }
        ]
      };

    case DELETE_ITEM:
      return {
        ...state,
        items: state.items.filter(item => item.id !== action.itemID)
      };

    case TOGGLE_ITEM:
      return {
        ...state,
        items: state.items.map(item => {
          return item.id === action.itemID
            ? { ...item, isComplete: !item.isComplete }
            : item;
        })
      };

    case TOGGLE_COMPLETED_ITEMS:
      return {
        ...state,
        showCompletedItems: !state.showCompletedItems
      };

    default:
      return state;
  }
};

export default reducer;
